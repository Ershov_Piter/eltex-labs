/*
 ============================================================================
 Name        : Lab2.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
#include <string.h>
#define ml 1024


int inp_str(char* st)
{
	int ls = 0;
	scanf("%s", st);
	ls = strlen(st);
	return ls;
}

void out_str(char* st, int *le, int *num)
{
	printf("\n");
	printf("Строка №%d ", *num + 1);
	printf(" %s", st);
	printf(" длины %d", *le);
	printf("\n");
}

char** readMas(int *c, char *st)
{
	int ls = 0;
	char **mas;

	mas = (char **)malloc(sizeof(char *)**c);
  	for (int i = 0; i < *c ; i++)
   	{
    		ls = inp_str(st);
        	mas[i] = (char *)malloc(sizeof(char)*ls);
       		strcpy(mas[i], st);
    	}
    	return mas;
}

void printMas(char **mas, int *count,  char *st)
{
    for (int i = 0; i < *count ; i++)
    {
    	strcpy(st, mas[i]);
    	out_str(st, &strlen(mas[i]), &i);
    }
}

void freeMas(char **mas, int *count)
{
	for (int i = 0; i < *count; i++)
	{
        free(mas[i]);
    }
    free(mas);
}

int sort(char **m, int *c)
{
	int  i = 0, f = 0, per = 0;
	char *p;

	for(i = *c - 1; i >= 0; i--)
	{
		f = 1;
		for(int j = 0; j < i; j++)
		{
			if( strlen(m[j]) > strlen(m[j + 1]) )
			{
				p = m[j];
				m[j] = m[j+1];
				m[j+1] = p;
				f = 0;
				per++;
			}
		}
		if (f == 1)
			break;
	}
	return per;
}

int main(int argc, char **argv)
{

	int c = 0, per = 0;
	char st[ml];
	char **m = NULL;

	printf("Введите число строк\n");
	scanf("%d", &c);

	mtrace();
	m = readMas(&c, st);
	printMas(m, &c, st);
	per = sort(m, &c);
	printMas(m, &c, st);

	printf("Число перестановок = %d \n", per);
	printf("Длина наименьшей строки = %d \n", strlen(m[0]));

	freeMas(m, &c);
}

