/*
 ============================================================================
 Name        : Lab2.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> /* для mtrace */
#include <string.h>
#define ml 1024 /* максимальная длина строки */


int inp_str(char* st)
{
	int ls = 0;
	scanf("%s", st); // читаем строку в буфер
	ls = strlen(st);
	return ls;
}

void out_str(char* st, int le, int num)
{
	printf("\n");
	printf("Строка №%d ", num + 1);
	printf(" %s", st);
	printf(" длины %d", le);
	printf("\n");
}

char** readMas(int c, char *st)
{
	int ls = 0;
	char **mas;  //указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*c);// выделяем память для массива указателей
	for (int i = 0; i < c ; i++)
   	{
    		ls = inp_str(st);
        	mas[i] = (char *)malloc(sizeof(char)*ls); //выделяем память для строки
        	strcpy(mas[i], st); //копируем строку из буфера в массив указателей
    	}
    	return mas;
}

void printMas(char **mas, int count,  char *st)
{
    for (int i = 0; i < count ; i++)
    {
    	strcpy(st, mas[i]);
    	out_str(st, strlen(mas[i]), i);
    }
}

void freeMas(char **mas, int count)
{
	for (int i = 0; i < count; i++)
	{
        	free(mas[i]); // освобождаем память для отдельной строки
    	}
   	free(mas); // освобождаем памать для массива указателей на строки
}

int sort(char **m, int c)
{
	int  i = 0, f = 0, per = 0;
	char *p;

	for(i = c - 1; i >= 0; i--)
	{
		f = 1;
		for(int j = 0; j < i; j++)
		{
			if( strlen(m[j]) > strlen(m[j + 1]) )
			{
				p = m[j];
				m[j] = m[j+1];
				m[j+1] = p;
				f = 0;
				per++;
			}
		}
		if (f == 1)
			break;
	}
	return per;
}

int main(int argc, char **argv)
{

	int c = 0, per = 0;
	char st[ml];
	char **m = NULL; //указатель на массив указателей на строки

	printf("Введите число строк\n");
	scanf("%d", &c);

	mtrace();

	m = readMas(c, st);
	printMas(m, c, st);
	per = sort(m, c);
	printMas(m, c, st);

	printf("Число перестановок = %d \n", per);
	printf("Длина наименьшей строки = %d", strlen(m[0]));

	freeMas(m, c);
}

