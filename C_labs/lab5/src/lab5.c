/*
 ============================================================================
 Name        : lab5.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

extern int plus();
extern int minus();

void *library_handler;

void main()
{

	//......
	//загрузка библиотеки
	library_handler = dlopen("/home/user/eclipse-workspace/lab5/src/libpmdyn.so",RTLD_LAZY);
	if (!library_handler){
		//если ошибка, то вывести ее на экран
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		exit(1); // в случае ошибки можно, например, закончить работу программы
	};
	int c1 = 0, c2 = 0;
	c1 = plus();
	c2 = minus();
	printf("c1 = %d\n", c1);
	printf("c2 = %d", c2);
}
