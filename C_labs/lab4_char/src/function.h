#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 1024


void readf(FILE *fr, char *ch, int *s) //получение массива символов из файла
{
	int i = 0;
	char b;
	while((b=fgetc(fr)) != EOF )
	{
		printf("%c", b);
		ch[i] = b;
		i++;
	}
	*s = i;
}

void printMas(char *mas, int *count) //вывод массива (вдруг нужно)
{
    for (int i = 0; i < *count ; i++)
    {
		printf("\n");
        printf("%c", mas[i]);
        printf("\n");
    }
}

void outf(FILE *fu, char *ch, char *b, int *s) //запись обработанного массива символов в файл
{
	for(int i = 0; i < *s; i++)
	{
		if(ch[i] != b[0])
		{
			printf("%c", ch[i]);
			fputc(ch[i], fu);
		}
	}
}
