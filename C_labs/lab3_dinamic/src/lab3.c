/*
 ============================================================================
 Name        : lab3.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "str.h"


void input(tab *tab, int ms)
{
	char st[1024];

	printf("введите фамилию\n");
	scanf("%s", &st);
	strcpy(tab->sf, st);
	printf("введите год рождения\n");
	scanf("%d", &tab->y);
	printf("введите номер отдела\n");
	scanf("%d", &tab->n);
	printf("введите размер оклада\n");
	scanf("%d", &tab->s);
}

void output(struct t *tab, int ms)
{
	printf("\n");
	printf("Фамилия %s\n", tab->sf);
	printf("Год рождения %d\n", tab->y);
	printf("Номер отдела %d\n", tab->n);
	printf("Оклад %d\n", tab->s);
	printf("\n");
}

static int cmp(const void *p1, const void *p2){
    struct t * st1 = *(struct t**)p1;
    struct t * st2 = *(struct t**)p2;
    return st1->y - st2->y;
}

void freeMas(char **mas, int ms) //очистка массива
{
	for (int i = 0; i < ms; i++)
	{
      		free(mas[i]);
   	}
	free(mas);
}


int main(void)
{
	struct t **sp;
	int ms = 0;

	printf("введите число записей\n");
	scanf("%d", &ms);

	sp = (struct t**)malloc(sizeof(struct t**)*ms);
	for(int i = 0; i < ms; i++)
	{
		sp[i]=(struct t*) malloc (sizeof(struct t));
		input(sp[i], ms);
	}

	for(int i = 0; i < ms; i++)
		output(sp[i], ms);

	qsort(sp, ms, sizeof(struct t*), cmp);

	for(int i = 0; i < ms; i++)
		output(sp[i], ms);
	freeMas(sp, ms);
	
}
