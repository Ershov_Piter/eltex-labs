/*
 ============================================================================
 Name        : lab4.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "function.h"

int main(int argc, char *argv[])
{

	FILE *fr, *fu;
	char s1[1024];
	char **ch = NULL;
	int buf = 0, s = 0;

	if((fr=fopen(argv[1],"a+"))==NULL)
	{
		printf("Невозможно открыть файл для чтения.\n");
		exit(1);
	}

	strcpy(s1, argv[1]);
	strncat(s1, ".txt", 4);

	if((fu=fopen(s1,"a+"))==NULL)
	{
		printf("Невозможно открыть файл для записи.\n");
		exit(1);
	}

	buf = atoi(argv[2]);

	ch = readf(fr, buf, &s);
	outf(fu, ch, buf, &s);
	freeMas(ch);

  	fclose(fr);
  	fclose(fu);

	return 0;
}
