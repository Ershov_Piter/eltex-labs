/*
 ============================================================================
 Name        : lab6.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "f.h"
#define N 1024


int main(int argc, char *argv[])
{

	FILE *fr;
	char s1[1024];
	char **ch = NULL;
	int s = 0;
	pid_t pid;

	if((fr=fopen(argv[1],"a+"))==NULL)
	{
		printf("Невозможно открыть файл для чтения.\n");
		exit(1);
	}

	strcpy(s1, argv[2]);

//	scanf("%s", &s1);
//	printf("%s = %d", s1,  strlen(s1));


	ch = readf(fr, &s);

//	printMas(ch, &s);

	pid = fork();
	if (-1 == pid)
	{
		perror("fork"); /* произошла ошибка */
		exit(1); /*выход из родительского процесса*/
	}
	else
		if (0 == pid)
		{
			search(ch, s1,&s);
			freeMas(ch);
			exit(EXIT_SUCCESS);
		}
//	printMas(ch, &s);

	freeMas(ch);
  	fclose(fr);

	return 0;
}
