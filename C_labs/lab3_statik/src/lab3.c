/*
 ============================================================================
 Name        : lab3.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "str.h"
#define mx 4

void input(elem tab[mx])
{
	elem *t;
	t = tab;

	printf("Введите 4 записи\n");

	for(int i = 0; i < 1; i++)
	{
		printf("введите фамилию\n");
		scanf("%s", t[i].sf);
		printf("введите год рождения\n");
		scanf("%d", t[i].year);
		printf("введите номер отдела\n");
		scanf("%d", t[i].n);
		printf("введите размер оклада\n");
		scanf("%d", t[i].sal);
	}
	tab = t;
}

void output(elem tab[mx])
{
	elem *t;
	t = tab;
	for(int i = 0; i < mx; i++)
	{
		printf("Фамилия %s\n", t[i].sf);
		printf("Год рождения %d\n", t[i].year);
		printf("Номер отдела %d\n", t[i].n);
		printf("Оклад %d\n", t[i].sal);
	}
}


int main(void)
{
	elem tab[mx];
	input(tab);
	output(tab);
}
